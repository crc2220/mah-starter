<?php 
//define constants//
define( 'THEMEROOT', get_template_directory_uri() );
define( 'IMAGES', THEMEROOT . '/img' );
define( 'STYLES', THEMEROOT . '/css' );
define( 'SCRIPTS', THEMEROOT . '/js' );



//THEME SUPPORT//
//add support for different post formats
add_theme_support( 'post-formats', 
  array(
    'gallery', 
    'link',
    'image',
    'quote', 
    'video',
    'audio'
    )
 );
//add support for automatic feed links
add_theme_support( 'automatic-feed-links' );
//add support for post thumbnails
add_theme_support( 'post-thumbnails' );
//add support for menus
add_theme_support('menus');
//register nav menus
register_nav_menus(
  array(
    'main-menu' => __( 'Main Menu', 'wmi' )
  )
);
//make theme available for translation//
$lang_dir = THEMEROOT . '/languages';
load_theme_textdomain( 'wmi', $lang_dir );



//CSS JS//
//css//
function load_css(){
	wp_enqueue_style( 'style', THEMEROOT . '/style.css' );
  wp_enqueue_style('fontawesome', 'http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css');
}    
add_action('wp_enqueue_scripts', 'load_css' );
//js//
function load_js() {

  wp_register_script( 'slidebars', SCRIPTS . '/vendor/slidebars.min.js', array('jquery'));
  wp_register_script( 'main', SCRIPTS . '/main.min.js', array('slidebars'));
  wp_enqueue_script('jquery');
  wp_enqueue_script('slidebars');
  wp_enqueue_script('main');

}
add_action( 'wp_enqueue_scripts', 'load_js' );





//WIDGETS//
function create_widget($name, $id, $description) {
	register_sidebar(array(
		'name' => __( $name , 'uikit' ),	 
		'id' => $id, 
		'description' => __( $description ),
		'before_widget' => ' ',
		'after_widget' => ' ',
		'before_title' => '<h5>',
		'after_title' => '</h5>'
	));
}
create_widget('Twitter', 'twitter', 'This is the twitter widget area');
create_widget('Newsletter', 'newsletter', 'This is the newsletter widget area');



//CUSTOM//
//custom featured image sizes
add_image_size('featuredImageNavList', 70, 70, true);

//excerpt limit//
function get_excerpt($count){
  $permalink = get_permalink($post->ID);
  $excerpt = get_the_content();
  $excerpt = strip_tags($excerpt);
  $excerpt = substr($excerpt, 0, $count);
  $excerpt = substr($excerpt, 0, strripos($excerpt, " "));
  $excerpt = $excerpt.'... <a href="'.$permalink.'">read more</a>';
  return $excerpt;
}

// read more 
function custom_excerpt_more($post) {
  return '<a href="'.get_permalink($post->ID).'" class="read-more"></br>'.'Continue Reading'.'</a>';
}
add_filter('excerpt_more', 'custom_excerpt_more');


//breadcrumbs//
function the_breadcrumb() {
    global $post;
    echo '<ul id="breadcrumbs">';
    if (!is_home()) {
        echo '<li><a href="';
        echo get_option('home');
        echo '">';
        echo 'Home';
        echo '</a></li><li class="separator"> / </li>';
        if (is_category() || is_single()) {
            echo '<li>';
            the_category(' </li><li class="separator"> / </li><li> ');
            if (is_single()) {
                echo '</li><li class="separator"> / </li><li>';
                the_title();
                echo '</li>';
            }
        } elseif (is_page()) {
            if($post->post_parent){
                $anc = get_post_ancestors( $post->ID );
                $title = get_the_title();
                foreach ( $anc as $ancestor ) {
                    $output = '<li><a href="'.get_permalink($ancestor).'" title="'.get_the_title($ancestor).'">'.get_the_title($ancestor).'</a></li> <li class="separator">/</li>';
                }
                echo $output;
                echo '<strong title="'.$title.'"> '.$title.'</strong>';
            } else {
                echo '<li><strong> '.get_the_title().'</strong></li>';
            }
        }
    }
    elseif (is_tag()) {single_tag_title();}
    elseif (is_day()) {echo"<li>Archive for "; the_time('F jS, Y'); echo'</li>';}
    elseif (is_month()) {echo"<li>Archive for "; the_time('F, Y'); echo'</li>';}
    elseif (is_year()) {echo"<li>Archive for "; the_time('Y'); echo'</li>';}
    elseif (is_author()) {echo"<li>Author Archive"; echo'</li>';}
    elseif (isset($_GET['paged']) && !empty($_GET['paged'])) {echo "<li>Blog Archives"; echo'</li>';}
    elseif (is_search()) {echo"<li>Search Results"; echo'</li>';}
    echo '</ul>';
}



//get categories related to current post in the loop 
function the_category_unlinked($separator = ' ') {
    $categories = (array) get_the_category();
    
    $thelist = '';
    foreach($categories as $category) {    // concate
        $thelist .= $separator . $category->category_nicename;}
  
    echo $thelist;
}




//ADMIN VIEW//
// add a favicon for your admin
function admin_favicon() {
	echo '<link rel="icon" type="image/ico" href="'. IMAGES .'/icons/favicon-admin.ico" />';
}
add_action('admin_head', 'admin_favicon');
// custom admin login logo
function custom_login_logo() {
	echo '<style type="text/css">
	h1 a { background-image: url('.get_bloginfo('template_directory').'/img/custom-login-logo.png) !important; }
	</style>';
}
add_action('login_head', 'custom_login_logo');


?>