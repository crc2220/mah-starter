var gulp = require('gulp'),
    sass = require('gulp-ruby-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    minifycss = require('gulp-minify-css'),
    jshint = require('gulp-jshint'),
    uglify = require('gulp-uglify'),
    rename = require('gulp-rename'),
	notify = require('gulp-notify'),
    imagemin = require('gulp-imagemin'),
    pngcrush = require('imagemin-pngcrush'),
    concat = require('gulp-concat'),
	size = require('gulp-size'),
    psi = require('psi'),
    site = 'http://localhost/bowl.com',
    key = '';

//sass
gulp.task('sass', function() {
  return gulp.src('sass/style.scss')
    .pipe(sass({ style: 'expanded' }))
    .pipe(autoprefixer('last 3 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'))
    .pipe(gulp.dest('css'))
    .pipe(minifycss())
    .pipe(gulp.dest('./'))
    .pipe(size())
    .pipe(notify({ message: 'Styles task complete' }));
});
//js
gulp.task('js', function() {
  return gulp.src('js/main.js')
    .pipe(jshint())
    .pipe(jshint.reporter('default'))
    .pipe(gulp.dest('js'))
    .pipe(rename({suffix: '.min'}))
    .pipe(uglify()) 
    .pipe(gulp.dest('js'))
	.pipe(size())
    .pipe(notify({ message: 'Scripts task complete' }));
});
//img
gulp.task('img', function () {
    return gulp.src('img/dev/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngcrush()]
        }))
        .pipe(gulp.dest('img/'))
        .pipe(size())
        .pipe(notify({ message: 'img optimized' }));
});
//psi testing
// gulp.task('mobile', function (cb) {
//     psi({
//         // key: key
//         nokey: 'true',
//         url: site,
//         strategy: 'mobile',
//     }, cb);
// });
// gulp.task('desktop', function (cb) {
//     psi({
//         nokey: 'true',
//         // key: key,
//         url: site,
//         strategy: 'desktop',
//     }, cb);
// });
//run all tasks
gulp.task('all', function() {
    gulp.start('sass', 'js');
});
//run gulp tasks on file change
gulp.task('watch', function() {
  gulp.watch('sass/style.scss', ['sass']);
  gulp.watch('js/main.js', ['js']);
});
