jQuery(document).ready(function($) {
    $.slidebars();

    $menu = $('.menu');

    $winWidth = $(window).width();

    var mobileNavCheck = function(){
    	if($winWidth < 821 ){
	    	$menu.appendTo('.sb-slidebar');
	    } else if ($winWidth > 821 ){
	    	$menu.appendTo('.nav');
	    }
	};

	mobileNavCheck();

    $(window).resize(function(){
    	$winWidth = $(window).width();
    	mobileNavCheck();
    });


});


