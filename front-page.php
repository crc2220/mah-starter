<?php get_header(); ?>
<div id="sb-site">
<div class="lanes-bg"></div>
	<div class="row main">
		<div class="column-12-hand">
		<div class="slider row">
				<div class="slide column-12-hand center-hand">
					<img src="<?php echo IMAGES ?>/clip-group-compressor.png" width="960" height="608">
				</div>
			</div>
			<div class="nav-bg row">
				<div class="column-12-hand column-6-lap">
					<a class="logo" href="<?php  echo esc_url( home_url('/')); ?>" rel="home">
						<img src="<?php echo IMAGES ?>/logo.png" width="306" height="163">
					</a>
				</div>
				<div class="nav column-12-hand column-6-lap">
					<i class="sb-toggle-left fa fa-bars"></i>
					<?php wp_nav_menu( $args ); ?>
				</div>
			</div>
			<div class="location row end-hand">
				<div class="column-12-hand column-6-lap">
					<div class="form">
						<select name="" id="">
							<option value="">Select A Location</option>
						</select>
						<h4>Pick a Location</h4>
					</div>
				</div>
			</div>
			</div>
		</div>
		<div class="ctas row">
			<div class="party column-9-hand column-5-lap">
				<img src="<?php echo IMAGES ?>/party.png" width="419" height="283">
			</div>
			<div class="column-12-hand column-2-lap">
				<img class="book-your-party" src="<?php echo IMAGES ?>/book-your-party.png" width="112" height="141">
			</div>
			<div class="column-12-hand column-5-lap">
				<div class="signup">
					<p>
						<span class="line-1">Stay up to date with the latest</span>
						<span class="line-2">Bowl America News!</span>
						<span class="line-3">Join our Email Club for Updates, FREE Offers & More.</span>
					</p>
					
					<div class="news-signup">
						<input type="text">
						<input type="submit">
					</div>

					<img class="pins" src="<?php echo IMAGES ?>/pins.png" width="182" height="116">

				</div>
			</div>
		</div>
	</div>
	<div class="row footer">
		<div class="column-12-hand column-6-lap">
			<div class="copyright">
				<p>Copyright © 2014 Bowl America, Inc.</p>
			</div>
		</div>
		<div class="column-12-hand column-6-lap">
			<div class="sitemap">
				<ul>
					<li><a href="sitemap">Sitemap</a></li>
					<li><a href="contact">Contact</a></li>
					<li><a href="about">About</a></li>
					<li><a href="investor">Investor Relations</a></li>
				</ul>
			</div>
		</div>
		</div>
	</div>
</div><!-- end of sb-site -->
<div class="sb-slidebar sb-left">
      <!-- Your left Slidebar content. -->
</div>
<?php get_footer(); ?>